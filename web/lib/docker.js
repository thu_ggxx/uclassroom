var process = require('child_process');
var colors = require('colors');
var fs = require('fs');
var config = JSON.parse(fs.readFileSync('./public/config.json'));
var git = require('./git.js');
var util = require('./util.js');


// source docker image name is teacher_name/lab_name
// target docker image name is student_name/lab_name-docker_guid
// public lab repository is teacher_name/lab_project
// private lab repository is student_name/docker_name

function getTargetDockerImageName(docker) {
	return docker.builder.name + '/' + docker.lab.name + '-' + docker.guid;
}

function getSourceDockerImageName(lab) {
	return lab.teacher.name + '/' + lab.name;
}

function createTTYJSConfig(docker) {
		var text = 
'{'+
'\n  "users": {'+
'\n    "' + docker.builder.name + '": "' + docker.builder.password + '"'+
'\n  },'+
'\n  "port": 8080,'+
'\n  "hostname": "0.0.0.0",'+
'\n  "shell": "bash",'+
'\n  "limitGlobal": 10000,'+
'\n  "limitPerUser": 1000,'+
'\n  "localOnly": false,'+
'\n  "cwd": ".",'+
'\n  "syncSession": false,'+
'\n  "sessionTimeout": 600000,'+
'\n  "log": true,'+
'\n  "io": { "log": false },'+
'\n  "debug": false,'+
'\n  "term": {'+
'\n    "termName": "xterm",'+
'\n    "geometry": [80, 24],'+
'\n    "scrollback": 1000,'+
'\n    "visualBell": false,'+
'\n    "popOnBell": false,'+
'\n    "cursorBlink": false,'+
'\n    "screenKeys": false,'+
'\n    "colors": ['+
'\n      "#2e3436",'+
'\n      "#cc0000",'+
'\n      "#4e9a06",'+
'\n      "#c4a000",'+
'\n      "#3465a4",'+
'\n      "#75507b",'+
'\n      "#06989a",'+
'\n      "#d3d7cf",'+
'\n      "#555753",'+
'\n      "#ef2929",'+
'\n      "#8ae234",'+
'\n      "#fce94f",'+
'\n      "#729fcf",'+
'\n      "#ad7fa8",'+
'\n      "#34e2e2",'+
'\n      "#eeeeec"'+
'\n    ]'+
'\n  }'+
'\n}';
}

function createDockerfileStep3(docker) {
	var text = 
		'# ' + docker.lab.name +
		'\n#' +
		'\n# VERSION    0.0.1' +
		'\n' +
		'\nFROM ' + docker.lab.teacher.name + '/' + docker.lab.name +
		'\nMAINTAINER Guo Xu <ggxx120@gmail.com>' +
		'\n' +
		'\nADD id_rsa /root/.ssh/' +
		'\nADD id_rsa.pub /root/.ssh/' +
		'\n' +
		'\nRUN git config --global user.name "' + docker.builder.name + '" ;\\' +
		'\n  git config --global user.email "' + docker.builder.email + '" ;\\' +
		'\n  chmod 0600 /root/.ssh/id_rsa ;\\' +
		'\n  echo -ne "StrictHostKeyChecking no\\nUserKnownHostsFile /dev/null\\n" >> /etc/ssh/ssh_config ;\\' +
		'\n  mkdir /' + docker.lab.name + ' ;\\' + 
		'\n  cd /' + docker.lab.name + ' ;\\' +
		'\n  git init ;\\' +
		'\n  wget -q -O /' + docker.lab.name + '/archive.tar.gz http://' + config.GIT.HOST + ':' + config.GIT.PORT + '/' + docker.lab.teacher.name + '/' + docker.lab.project + '/repository/archive.tar.gz ;\\' +
		'\n  tar -xzf /' + docker.lab.name + '/archive.tar.gz -C /' + docker.lab.name + '/ ;\\' +
		'\n  cp -r /' + docker.lab.name + '/' + docker.lab.project + '.git/* /' + docker.lab.name + '/ ;\\' +
		'\n  rm -r /' + docker.lab.name + '/' + docker.lab.project + '.git ;\\' +
		'\n  rm /' + docker.lab.name + '/archive.tar.gz ;\\' +
		'\n  cd /' + docker.lab.name + ' ;\\' +
		'\n  git add . ;\\' +
		'\n  git remote add origin git@' + config.GIT.HOST + ':' + docker.builder.name + '/' + docker.lab.name + '.git ;\\' +
		'\n  git commit -a -s -m "init" ;\\' +
		'\n  git push -u origin master ;' +
		'\n' +
		'\n EXPOSE 8080' +
		'\n ENTRYPOINT ["tty.js", "--config", "/ttyjs-config.json"]';
	console.log(text);
	return text;
}

function _buildDocker(docker, callback) {
	
	var projectName = docker.lab.name;// + '_' + docker.url.substring(0, 4); //uncomment if allow user to create more than one project for one lab
	var path = '/tmp/uclassroom';
	var dockerRoot = path + '/docker';
	var dockerPath = dockerRoot + '/' + docker.guid;
	var idRsaFile = dockerPath + '/id_rsa';
	var idRsaPubFile = dockerPath + '/id_rsa.pub';
	var dockerFile = dockerPath + '/Dockerfile';
	//var dockerFileData = createDockerfile(docker.lab, { projectName: projectName, userName: docker.builder.name, email: docker.builder.email });
	var dockerFileData = createDockerfileStep3(docker);
	
	// Make dir, prepare user Dockerfile & SSH key
	fs.exists(path, function(exists) {
		if (!exists) {
			fs.mkdirSync(path); 
		}
		fs.exists(dockerRoot, function(exists) {
			if (!exists) {
				fs.mkdirSync(dockerRoot);
			}
			fs.mkdirSync(dockerPath);
			fs.appendFileSync(dockerFile, dockerFileData);
			fs.appendFileSync(idRsaFile, docker.builder.privateKey);
			fs.appendFileSync(idRsaPubFile, docker.builder.publicKey);
			
			// don't fork the lab project 'cause it sould be private rather than public
			// just create a new private project and add teacher to the project as developer. stupid maybe...
			git.createPrivateProject(config.GIT.HOST, config.GIT.PORT, docker.builder.gitToken, projectName, function(a) {
				git.addProjectDeveloper(config.GIT.HOST, config.GIT.PORT, docker.builder.gitToken, docker.builder.name + '%2F' + projectName, docker.lab.teacher.gitId, function(b) {
					// build docker (step3)
					var dockerImageName = getTargetDockerImageName(docker);
					var cmd_build = 'cd ' + dockerPath + ' && docker build --rm -t "' + dockerImageName + '" .';
					console.log('[exec] '.yellow + cmd_build.yellow);
					process.exec(cmd_build, function (error, stdout, stderr) {
						if (error !== null) {
							console.log('exec error: ' + error);
						}
						else {
							callback('ok');
						}
					});
				});
			});
		});
	});
}

function _buildBaseDocker(lab, callback) {
	
	var guid = util.guid();
	var path = '/tmp/uclassroom';
	var dockerRoot = path + '/docker';
	var dockerPath = dockerRoot + '/' + guid;
	var dockerFile = dockerPath + '/Dockerfile';
	var dockerFileData = lab.dockerFile;
	
	fs.exists(path, function(exists) {
		if (!exists) {
			fs.mkdirSync(path); 
		}
		fs.exists(dockerRoot, function(exists) {
			if (!exists) {
				fs.mkdirSync(dockerRoot);
			}
			fs.mkdirSync(dockerPath);
			fs.appendFileSync(dockerFile, dockerFileData);
			
			var dockerImageName = getSourceDockerImageName(lab);
			var cmd_build = 'cd ' + dockerPath + ' && docker build --rm -t "' + dockerImageName + '" .';
			console.log('[exec] '.yellow + cmd_build.yellow);
			process.exec(cmd_build, function (error, stdout, stderr) {
				if (error !== null) {
					console.log('exec error: ' + error);
				}
				else {
					console.log('docker is readly');
					callback('ok');
				}
			});
		});
	});
}

function _runDocker(docker, callback) {
	// run docker
	var cmd = 'docker run -d -p :8080 ' + getTargetDockerImageName(docker);
	console.log('[exec] '.yellow + cmd.yellow);
	process.exec(cmd, function (error, stdout, stderr) {
		if (error !== null) {
			console.log('exec error: ' + error);
		}
		else {
			docker.lastRunTime = new Date();
			docker.running = true;
			docker.contId = stdout.toString();
			
			cmd = 'docker port ' + docker.contId;
			console.log('[exec] '.yellow + cmd.yellow);
			process.exec(cmd, function (error2, stdout2, stderr2) {
				if (error2 !== null) {
					console.log('exec error: ' + error2);
				}
				else {
					//stdout2 "8080/tcp -> 0.0.0.0:49153"
					console.log(stdout2.toString().split(":")[1]);
					docker.host = config.DOCKER.HOST;
					docker.port = stdout2.toString().split(":")[1];
					callback();
				}
			});
		}
	});
	
	//TODO: set proxy
}

function _stopDocker(docker, callback) {
	// stop docker
	var cmd = 'docker stop ' + docker.contId;
	console.log('[exec] '.yellow + cmd.yellow);
	process.exec(cmd, function (error, stdout, stderr) {
		if (error !== null) {
			console.log('exec error: ' + error);
		}
		else {
			docker.running = false;
			docker.contId = '';
			callback();
		}
	});
	
	//TODO: stop proxy
}

exports.builBaseDocker = _buildBaseDocker;
exports.buildDocker = _buildDocker;
exports.runDocker = _runDocker;
exports.stopDocker = _stopDocker;
