var fs = require('fs');
var express = require('express');
var util = require('../lib/util.js');
var router = express.Router();
var models = require('../lib/model.js');
var db = require('../lib/db.js');
var dockerUtil = require('../lib/docker.js');
var git = require('../lib/git.js');
var config = JSON.parse(fs.readFileSync('./public/config.json'));

router.get('/', function(req, res) {
	if (util.isEmpty(req.session.userid)) {
		res.redirect('/login');
		return;
	}
	db.getLabs(function(result_labs) {
		res.render('labs.ejs', { title: config.TITLE, username: req.session.username, labs: result_labs, message: '' });
	});
});

router.get('/:labid', function(req, res) {
	if (util.isEmpty(req.session.userid)) {
		res.redirect('/login');
		return;
	}
	db.findUser(req.session.userid, function(result_user) {
		git.getUserProjects(config.GIT.HOST, config.GIT.PORT, result_user.gitToken, function(result_projects) {
			var labid = req.params.labid;
			if (labid.length == 12 || labid.length == 24) {	// mongodb pk length
				db.findLab(labid, function(result_lab) {
					if (util.isEmpty(result_lab)) {
						res.render('lab.ejs', { title: config.TITLE, username: req.session.username, lab: new models.Lab(), projects: result_projects, message: '' });
					}
					else {
						res.render('lab.ejs', { title: config.TITLE, username: req.session.username, lab: result_lab, projects: result_projects, message: '' });
					}
				});
			}
			else {
				res.render('lab.ejs', { title: config.TITLE, username: req.session.username, lab: new models.Lab(), projects: result_projects, message: '' });
			}
		});
	});
});

router.post('/:labid', function(req, res) {
	
	if (util.isEmpty(req.session.userid)) {
		res.redirect('/login');
		return;
	}
	
	db.findUser(req.session.userid, function(result_user) {
		git.getUserProjects(config.GIT.HOST, config.GIT.PORT, result_user.gitToken, function(result_projects) {
			var labid = req.params.labid;
			db.findLab(labid, function(result_lab) {
				result_lab.name = req.body.name;
				result_lab.desc = req.body.desc;
				result_lab.dockerFile = req.body.dockerFile;
				result_lab.dockerType = req.body.dockerType;
				result_lab.teacher = result_user;
				result_lab.project = req.body.project;
				result_lab.creatingTime = new Date();
				result_lab.makeScripts = req.body.makeScripts;
				
				db.upsertLabByName(result_lab, function(result) {
					res.redirect('/labs?from=saveok');
				});
			});
		});
	});
});


router.post('/', function(req, res, next) {
	
	if (util.isEmpty(req.session.userid)) {
		res.redirect('/login');
		return;
	}
	
	db.findUser(req.session.userid, function(result_user) {
		var lab = new models.Lab();
		lab.name = req.body.name;
		lab.desc = req.body.desc;
		lab.dockerFile = req.body.dockerFile;
		lab.dockerType = req.body.dockerType;
		lab.teacher = result_user;
		lab.project = req.body.project;
		lab.creatingTime = new Date();
		lab.makeScripts = req.body.makeScripts;
		lab.status = 'creating';
		
		db.validateLab(lab, function(err) {
			if (err != '') {
				res.render('lab.ejs', { title: config.TITLE, username: req.session.username, lab: lab, message: err });
				return;	
			}
			db.upsertLabByName(lab, function(result) {
				//build docker (step2)
				dockerUtil.builBaseDocker(lab, function(msg) {
					db.getLabByName(lab.name, function(result_lab) {
						result_lab.status = 'ready';
						db.updateLab(result_lab, function(result2) {
							console.log('lab saved');
						});
					});		
				});
				res.redirect('/labs?from=saveok');
			});
		});
	});
});

module.exports = router;
