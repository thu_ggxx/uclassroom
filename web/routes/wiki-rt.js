var express = require('express');
var fs = require('fs');
var router = express.Router();
var config = JSON.parse(fs.readFileSync('./public/config.json'));

router.get('/', function(req, res) {
	res.render('readme.ejs', { title: config.TITLE, username: req.session.username });
});

module.exports = router;
