var fs = require('fs');
var express = require('express');
var router = express.Router();
var db = require('../lib/db.js');
var util = require('../lib/util.js');
var config = JSON.parse(fs.readFileSync('./public/config.json'));

router.get('/', function(req, res) {
	if (util.isEmpty(req.session.userid)) {
		res.render('login.ejs', { title: config.TITLE, name: '', message: '' } );
	}
	else {
		res.redirect('/');
	}
});

router.post('/', function(req, res, next) {
	db.login(req.body.name, util.sha1(req.body.password), function(user) {
		if (!util.isEmpty(user)) {
			req.session.userid = user._id;
			req.session.username = user.name;
			res.redirect('/');
		}
		else {
			res.render('login.ejs', { title: config.TITLE, name: req.body.name, message: 'Username or password is incorrect.' } );
		}
	});
});

module.exports = router;
