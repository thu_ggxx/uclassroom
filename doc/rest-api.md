uClassroom Documentation
==========

# REST API

## Users

### List users

Get a list of users.

```
GET /users
```

```
[
  {
  }
]
```

### Single User

Get a single user.

```
GET /users/:id
```

Parameters:

- id(required) - The ID of user

```
{
}
```

### User creation
Creates a new user.

```
POST /users
```

Parameters:

- email (required) - Email
- password (required) - Password
- username (required) - Username
- name (required) - Name
- skype (optional) - Skype ID
- linkedin (optional) - LinkedIn
- twitter (optional) - Twitter account
- website_url (optional) - Website URL
- projects_limit (optional) - Number of projects user can create
- extern_uid (optional) - External UID
- provider (optional) - External provider name
- bio (optional) - User's biography
- admin (optional) - User is admin - true or false (default)
- can_create_group (optional) - User can create groups - true or false

### User modification
Modifies an existing user.

```
PUT /users/:id
```

Parameters:

- id (required) - The ID of user
- email - Email
- username - Username
- name - Name
- password - Password
- skype - Skype ID
- linkedin - LinkedIn
- twitter - Twitter account
- website_url - Website URL
- projects_limit - Limit projects each user can create
- extern_uid - External UID
- provider - External provider name
- bio - User's biography
- admin (optional) - User is admin - true or false (default)
- can_create_group (optional) - User can create groups - true or false


## Classrooms

### List classrooms

Get a list of classroooms

```
GET /rooms
```

```
[
  {
  }
]
```

### Single classroom

Get a single classroom

```
GET /rooms/:id
```

Parameters:

- id (required) - The ID of user

```
{
}
```

### Classroom creation

Creates a new classroom.

```
POST /rooms
```

Parameters:

- room_name (required) - The name of room


## Building Results

### List building results of user's codes

Get a list of building results of user's codes.

```
GET /results/:username
```

Parameters:

- username (required) - The name of user

```
[
  {
  }
]
```

### Add Building Result

Add a building result.

```
POST /results
```

Parameters:

- docker_id (required) - The id of docker
- pushing_time (required)
- building_time (required)
- grade 
- desc

```
{
}
```

