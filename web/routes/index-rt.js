var fs = require('fs');
var express = require('express');
var session = require('express-session');
var util = require('../lib/util.js');
var login = require('../routes/login-rt.js');
var config = JSON.parse(fs.readFileSync('./public/config.json'));
var router = express.Router();

router.get('/', function(req, res) {
	if (!util.isEmpty(req.session.userid)) {
		res.render('index.ejs', { title: config.TITLE, username: req.session.username });
	}
	else {
		res.redirect('/login');
	}
});

module.exports = router;
