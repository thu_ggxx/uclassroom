var colors = require('colors');
var cookieParser = require('cookie-parser');
var socketHandshake = require('socket.io-handshake');
var util = require('./util.js');
var models = require('./model.js');
var dockerUtil = require('./docker.js');

function _listen(io, db, sessionStore, config) {
	var nsp = io.of('/cloud');
	nsp.use(socketHandshake( {store: sessionStore, key: config.SESSION_KEY, secret: config.SESSION_SECRET, parser: cookieParser()} ));
	nsp.on('connection', function(socket) {
		console.log('socket on [connection]'.blue);
		
		if (util.isEmpty(socket.handshake.session.userid)) {
			socket.emit('not_login');
			return;
		}
		
		var userid = socket.handshake.session.userid;
		emitDockers();
		
		socket.on('dockers', function() {
			emitDockers();
		});
		
		socket.on('build_docker', function(message) {
			if (util.isEmpty(message.dockerLab) || util.isEmpty(message.dockerName) || (message.dockerLab.length != 12 && message.dockerLab.length != 24)) {
				socket.emit('build_docker');
				return;
			}
			db.findUser(userid, function(result_user) {
				db.findLab(message.dockerLab, function(result_lab) {
					var docker = new models.Docker();
					docker.name = message.dockerName;
					docker.lab = result_lab;
					docker.builder = result_user;
					docker.startBuildTime = new Date();
					docker.status = 'building';
					docker.guid = util.guid();
					db.insertDocker(docker, function(result_num) {
						emitDockers();
						dockerUtil.buildDocker(docker, function() {
							db.getDockerByGuid(docker.guid, function(result_docker) {
								result_docker.buildTime = new Date();
								result_docker.status = 'ready';
								db.updateDocker(result_docker, function(result_num) {
									emitDockers();
								});
							});
						});
					});
				});
			});
		});
		
		socket.on('start_docker', function(dockerid) {
			db.findDocker(dockerid, function(result_docker) {
				result_docker.status = 'operating';
				db.updateDocker(result_docker, function(result_num) {
					emitDockers();
					dockerUtil.runDocker(result_docker, function() {
						result_docker.status = 'ready';
						db.updateDocker(result_docker, function(result_num) {
							emitDockers();
						});
					});
				});
			});
		});
		
		socket.on('stop_docker', function(dockerid) {
			db.findDocker(dockerid, function(result_docker) {
				result_docker.status = 'operating';
				db.updateDocker(result_docker, function(result_num) {
					emitDockers();
					dockerUtil.stopDocker(result_docker, function() {
						result_docker.status = 'ready';
						db.updateDocker(result_docker, function(result_num) {
							emitDockers();
						});
					});
				});
			});
		});
		
		function emitDockers() {
			db.getUserDockers(userid, function(result_dockers) {
				socket.emit('dockers', result_dockers);
			});
		}
	});
}

exports.listen = _listen;
