var express = require('express');
var fs = require('fs');
var models = require('../lib/model.js');
var git = require('../lib/git.js');
var db = require('../lib/db.js');
var util = require('../lib/util.js');
var config = JSON.parse(fs.readFileSync('./public/config.json'));
var router = express.Router();

router.get('/', function(req, res) {
	var pars = {
		title: config.TITLE,
		name: '',
		email: '',
		role: 'Student',
		message: ''
	};
	res.render('signup.ejs', pars);
});

router.post('/', function(req, res, next) {
	
	var user = new models.User();
	user.name = req.body.name;
	user.password = util.sha1(req.body.password);
	user.creatingTime = new Date();
	user.email = req.body.email;
	user.role = req.body.role;
	
	// Check user before insert
	db.validateUser(user, function(err) {
		if (err == '') {
			//Add git account and get git-id
			git.createGitAccount(config.GIT.HOST, config.GIT.PORT, config.GIT.TOKEN, user.email, req.body.password, user.name, user.name, function(body) {
				var gitMsg = JSON.parse(body);
				if (!util.isEmpty(gitMsg.message)) {
					var pars = {
						title: config.TITLE,
						name: user.name,
						email: user.email,
						role: user.role,
						message: JSON.stringify(gitMsg.message)
					};
					res.render('signup.ejs', pars);
					return;
				}
				user.gitId = gitMsg.id;
				// get git token
				git.session(config.GIT.HOST, config.GIT.PORT, user.name, req.body.password, function(resutl_body) {
					gitMsg = JSON.parse(resutl_body);
					user.gitToken = gitMsg.private_token;
					// Create ssh key
					util.genSSHKey(user.email, function(priKey, pubKey) {
						user.privateKey = priKey;
						user.publicKey = pubKey;
						// Add ssh key to git
						git.addSSHKey(config.GIT.HOST, config.GIT.PORT, config.GIT.TOKEN, user.gitId, 'uClassroomKey', user.publicKey, function() {
							// Add new user
							db.insertUser(user, function(user) {
								var pars = { 
									name: user.name,
									email: user.email,
									role: user.role,
									message: '',
									sign: true
								};
								res.redirect('/login?from=signupok');
								
								//var title = 'uClassroom - Message';
								//var body = 'You have successfully registered, please <a id="login" href="/login">log in</a>;'
								//var html = '<!DOCTYPE html><html><head><title>' + title + '</title></head><body>' + body + '</body></html>';
								//res.end(html);
							});
						});
					});
				});
			});
		}
		else {
			var pars = {
				title: config.TITLE,
				name: user.name,
				email: user.email,
				role: user.role,
				message: err
			};
			res.render('signup.ejs', pars);
		}
	});
});

module.exports = router;
