var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var fs = require('fs');
var http = require('http');
var https = require('https');
var colors = require('colors');
var sio = require('socket.io');

var db = require('./lib/db.js');
var util = require('./lib/util.js');
var rtc = require('./lib/rtc.js');
var cloudManager = require('./lib/cloudmgr.js');

var index = require('./routes/index-rt.js');
var login = require('./routes/login-rt.js');
var logout = require('./routes/logout-rt.js');
var signup = require('./routes/signup-rt.js');
var classroom = require('./routes/classroom-rt.js');
var homework = require('./routes/homework-rt.js');
var lab = require('./routes/lab-rt.js');
var wiki = require('./routes/wiki-rt.js');
var api = require('./routes/api-rt.js');
var cloud = require('./routes/cloud-rt.js');


var config = JSON.parse(fs.readFileSync('./public/config.json'));
var sessionStore = new session.MemoryStore( { reapInterval: 60000 * 10 } );
var options = {
	key: fs.readFileSync('./public/keys/agent-test-key.pem'),
	cert: fs.readFileSync('./public/keys/agent-test-cert.pem')
};

var app = express();
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
	key: config.SESSION_KEY,
	store: sessionStore,
	secret: config.SESSION_SECRET,
	resave: true,
	saveUninitialized: true
}));

app.use('/', index);
app.use('/login', login);
app.use('/logout', logout);
app.use('/signup', signup);
app.use('/classroom', classroom);
app.use('/homeworks', homework);
app.use('/labs', lab);
app.use('/wiki', wiki);
app.use('/api', api);
app.use('/cloud', cloud);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
			title: config.TITLE,
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
		title: config.TITLE,
        message: err.message,
        error: {
			status: err.status, 
			stack: '' 
		}
    });
});

// Start the application after the database connection is ready
var server = config.USE_HTTPS ? https.createServer(options, app) : http.createServer(app);
db.connect(config.DB_URL, function() {
	server.listen(config.PORT);
	var io = sio.listen(server);
	rtc.listen(io, db, sessionStore, config);
	cloudManager.listen(io, db, sessionStore, config);
	console.log('[info] '.green + 'start listening on port ' + config.PORT);
});

/*module.exports = app;*/
