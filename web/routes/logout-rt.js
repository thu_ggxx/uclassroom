var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	delete req.session.userid;
	delete req.session.username;
	res.redirect('/');
});

module.exports = router;
