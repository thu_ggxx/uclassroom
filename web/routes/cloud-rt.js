var fs = require('fs');
var express = require('express');
var util = require('../lib/util.js');
var db = require('../lib/db.js');
var router = express.Router();
var config = JSON.parse(fs.readFileSync('./public/config.json'));

router.get('/', function(req, res) {
	if (util.isEmpty(req.session.userid)) {
		res.redirect('/login');
		return;
	}
	
	db.getReadyLabs(function(result_labs) {
		if (util.isEmpty(result_labs)) {
			result_labs = [];
		}
		res.render('cloud.ejs', { title: config.TITLE, username: req.session.username, message: '', labs: result_labs });
	});
});

module.exports = router;
