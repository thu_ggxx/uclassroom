'use strict';

var password = document.getElementById('password');
var password2 = document.getElementById('password2');

function validatePassword(form) {
	with (form) {
		if (password.value != '' && password.value == password2.value) {
			return true;
		}
		alert("These passwords don't match. Try again?");
		return false;
	}
}
