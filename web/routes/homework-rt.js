var fs = require('fs');
var express = require('express');
var router = express.Router();
var util = require('../lib/util.js');
var db = require('../lib/db.js');
var config = JSON.parse(fs.readFileSync('./public/config.json'));

router.get('/', function(req, res) {
	if (util.isEmpty(req.session.userid)) {
		res.redirect('/login');
		return;
	}
	
	db.getUserResults(req.session.userid, function(result_results) {
		res.render('homeworks.ejs', { title: config.TITLE, username: req.session.username, results: result_results });
	});
});

router.get('/:resultid', function(req, res) {
	if (util.isEmpty(req.session.userid)) {
		res.redirect('/login');
		return;
	}
	var resultid = req.params.resultid;
	if (resultid.length == 12 || resultid.length == 24) {	// mongodb pk length
		db.findResult(resultid, function(result_result) {
			if (!util.isEmpty(result_result)) {
				res.render('homework.ejs', { title: config.TITLE, username: req.session.username, result: result_result, message: '' });
			}
		});
	}
});

module.exports = router;
